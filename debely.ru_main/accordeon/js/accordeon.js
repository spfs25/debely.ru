var title = document.getElementsByClassName('accordeon__trigger'),
    cont = document.getElementsByClassName('accordeon__inner-link');

for (var i = 0; i < title.length; i++) {

    title[i].addEventListener('click', function () {

        if (!(this.nextElementSibling.classList.contains('accordeon__inner_active'))) {
            for (var i = 0; i < title.length; i++) {
                title[i].nextElementSibling.classList.remove('accordeon__inner_active');
                title[i].previousElementSibling.classList.remove('accordeon__triangle_active');
                title[i].classList.remove('accordeon__trigger_active');
                title[i].nextElementSibling.style.maxHeight = null;
            }
            this.nextElementSibling.classList.add('accordeon__inner_active');
            this.previousElementSibling.classList.add('accordeon__triangle_active');
            this.classList.add('accordeon__trigger_active');
            this.nextElementSibling.style.maxHeight = this.nextElementSibling.scrollHeight + "px";
        } else {
            this.classList.remove('accordeon__trigger_active');
            this.nextElementSibling.style.maxHeight = null;
            this.previousElementSibling.classList.remove('accordeon__triangle_active');
            this.nextElementSibling.classList.remove('accordeon__inner_active');
        }
    })
}