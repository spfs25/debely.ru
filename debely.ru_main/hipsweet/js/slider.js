var slides = document.querySelectorAll('li.slides-list__item');
var switchBlocks = document.querySelectorAll('li.switch-list__item');
var headerLinks = document.querySelectorAll('a.submenu__link');


function showActiveSlide(Event){
    for(var i=0; i<switchBlocks.length; i++){
        if((Event.target===switchBlocks[i])||(Event.target===headerLinks[i])){
            switchBlocks[i].classList.add('switch-list__item_active');
            slides[i].classList.add('slides-list__item_active');
        }
        else{
            switchBlocks[i].classList.remove('switch-list__item_active');
            slides[i].classList.remove('slides-list__item_active');
        }
    }
}

for (var j=0; j<switchBlocks.length; j++){
    switchBlocks[j].addEventListener("click", showActiveSlide);
    headerLinks[j].addEventListener("click", showActiveSlide);
}
