$(document).ready(function(){
    
    var slides = document.querySelectorAll('li.slides-list__item');
    var switchBlocks = document.querySelectorAll('li.switch-list__item');
    var headerLinks = document.querySelectorAll('a.submenu__link');


    function showActiveSlide(Event){
        for(var i=0; i<switchBlocks.length; i++){
            if((Event.target===switchBlocks[i])||(Event.target===headerLinks[i])){
                switchBlocks[i].classList.add('switch-list__item_active');
                slides[i].classList.add('slides-list__item_active');
            }
            else{
                switchBlocks[i].classList.remove('switch-list__item_active');
                slides[i].classList.remove('slides-list__item_active');
            }
        }
    }

    for (var j=0; j<switchBlocks.length; j++){
        switchBlocks[j].addEventListener("click", showActiveSlide);
        headerLinks[j].addEventListener("click", showActiveSlide);
    }


    //mask
    $('#phone').mask('+7-000-000-00-00');

    //team-tabs
    
    (function(){
        
            $('.team-list__link').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                item = $this.closest('.team-list__item'),
                container = $this.closest('.team-tabs'),
                content = container.find('.team-content__item');
                ndx = item.index();

            item
                .addClass('team-list__item_active')
                .siblings().removeClass('team-list__item_active');

            content.eq(ndx)
                            .addClass('team-content__item_active')
                            .siblings().removeClass('team-content__item_active');



            });
	}());
    
    
    
    
    //accordion
    
    (function(){
        
        var flag = true;
        
        $('.accordion__link').on('click', function(e){
            e.preventDefault();
            
            
            var
                $this = $(this),
                container = $this.closest('.accordion__list'),
                item = $this.closest('.accordion__item'),
                currentContent = item.find('.accordion__content'),
                duration = 500;
            
            
            if(flag){
                flag=false;
                if(!item.hasClass('accordion__item_active')){

                    item
                        .addClass('accordion__item_active')
                        .siblings()
                        .removeClass('accordion__item_active')
                        .find('.accordion__content')
                        .slideUp(duration); 

                    currentContent.slideDown(duration, function(){
                        flag = true;
                    });

                }else{
                    item.removeClass('accordion__item_active');
                    currentContent.slideUp(function(){
                        flag = true;
                    });
                }
            }
                
            
            
        });
    })();
    
//map
    
    var myMap;
    ymaps.ready(init);

    function init () {
        myMap = new ymaps.Map('map', {
            center: [59.92111338788696, 30.308267835961054],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        });
        
        var myPlacemark = new ymaps.Placemark([59.92111338788696, 30.308267835961054], {}, {
            iconLayout: 'dx`efault#image',
            iconImageHref: 'img/map-section/map_icon.png',
            iconImageSize: [30, 42],
            iconImageOffset: [-3, -42]
        });
        
       
        
        myMap.geoObjects.add(myPlacemark);

    }
    
 
});